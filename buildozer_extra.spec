# Do not use inline comments on this file

# Linux Virtualmachine name
VMname = Ubuntu18_50gb

# drive mount string as letter, 'sf_C_DRIVE' for example would be C
vbox_mount_drive = C

# directory for storage on Linux Virtualmachine
vbox_dir = GitHub/bikeano

# Directory structure where buildozer stores .apk
#buildozer_ext_path = .buildozer/android/platform/python-for-android/dist
buildozer_ext_path = bin


# !!!! use jar_location, jar_dir, jar_name for singular, add (s) on to each for multiple !!!!
# !!!! quantity must be the same for all 3 !!!!

# Location of .class files for jar creation
# jar_location = C:/AndroidStudioProjects/antpluslistener/app/build/intermediates/classes/debug
jar_location = C:/AndroidStudioProjects/VeloViewAnt/app/build/intermediates/classes/debug

# directory structure of jar file
jar_dir = com/velosense/ant

# name of jar file to be created
# jar_name = antpluslistener.jar
jar_name = channel.jar

# Windows jar destination (do not include leading slash '/')
jar_destination_windows = AppJars

# Buildozer build line
# buildozer_build_line = buildozer android release <-- No longer used

# Zipalign for release cmd
release_zipalign_cmd = ~/Android/Sdk/build-tools/25.0.2/zipalign -f -v 4

# Zipalign for release extension:  apk_name + release_zipalign_ext + .apk
release_zipalign_ext = -optimized

# This is the extension which the buildozer build puts on the android activity
# the default is /org.renpy.android.PythonActivity
apk_activity = org.kivy.android.PythonActivity

# This is the identifier for the .apk file in case of duplicates
apk_filename_ext = *.apk

# List of files to copy to Virtualmachine
vbox_copy_files = *.py *.json *.jar *.aar *.json *.jpg buildozer.spec *.ini probe_cal*.npy *.ttf *.png

# List of files which will be excluded from copy
vbox_exclude_files = run_compile.py buildozer_tools.py script_params.json bike_collection.json

# List of directories to exclude
vbox_exclude_dirs = .git, _pycache_, com, files, VSdataview, org, Obsolete, OldScreens, Archive, WindowsComponents, AppBase/files
