from __future__ import division
import os.path
import time
import subprocess
import glob
import sys
import shutil
from multiprocessing import Process
import json

###################################################################################################
# This is a script for compiling a kivy project on windows in conjunction with virtualbox.
# It copies this file to an existing virtualbox guest session using pyvbox and the file
# copy_compile.py which is installed in the guest home dir.  It then copies the relevant directory
# code directory for the project to the guest machine and runs buildozer.  Upon complection, the
# .apk file is copied back to the windows machine, where it is then installed on the android device
# using ADB tools.
#
#         The requirements are:
#
#           pyvbox - https://pypi.python.org/pypi/pyvbox
#
#           Virtualbox with linux guest.  Buildozer must be installed on the guest.
#
#           Android Studio or some other version of ADB tools
#
#         The components of this script are:
#
#           buildozer_extra.spec sets the parameters for the script
#           See buildozer_tools.py and buildozer_extra.spec for further information.
#
#           make_jar() A script which fetches and compiles any jar files from a specified
#           directory.
#
#           copy_compile.py must be copied to the home directory of the virtualbox installation.
#           This script is run from pyvbox, currently the Virtualbox environment must be switched
#           on (but can be minimised) with a terminal window open.
#
#          To run the script:
#
#           python run_compile.py [options]
#
#               options:
#
#                   if omitted, runs debug script
#
#                   debug (default)
#                       copies files from current windows directory
#                       runs debug build
#                       copies back to winddows directory
#                       installs onto android and runs
#
#                   release
#                       As debug, but uses 'buildozer android release'
#                       (deprecated) aligns the zip file before copying
#
#                   make_jar
#                       As debug + Builds jar files from dirs specified in buildozer_extra.spec
#
#                   new_project
#                       As debug + Creates new directory on VM
#
#                   build
#                       as debug, without installation onto android
#
#                   copy_to_vm
#                       copies files to vm, does not start build
#
#                   return_copy_only
#                       Copies from VM to windows, installs, executes
#                       No build
#
#                   install
#                       Installs existing .apk to connected android device
#                       No build
#                       No install
#
#                   run_apk
#                       Only runs apk on connected android device
#                       No build
#                       No install
# 
#                   help
#                       prints out this doc
################################################################################
exec_buildozer = ['buildozer', 'android']
EXEC_BUILDOZER_DEBUG = exec_buildozer + ['debug']
EXEC_BUILDOZER_RELEASE = exec_buildozer + ['release']

P4A_KEYS = {'P4A_RELEASE_KEYSTORE': '/home/anon/keystores/Anon.keystore',
            'P4A_RELEASE_KEYSTORE_PASSWD': 'anonanon',
            'P4A_RELEASE_KEYALIAS_PASSWD': 'anonanon',
            'P4A_RELEASE_KEYALIAS': 'anonAlias'}


class VboxScript(object):
    """VboxScript exectutes the windows portion of the run_compile script.
    It parses and holds the parameters for buildozer_extra.spec.
    Also creates script_params.json which is copied to the VM
     """

    def __init__(self):
        """Parses buildozer_extra.spec and dispatches command based on arg parameters"""
        if 'help' in sys.argv:
            self.print_doc()
            return
        self.jars_exist = False
        self.timer = timer()
        print_help_hint()
        self.alt_vbox_dir = 'alt_dir' in sys.argv  # Allows alteranate windows dir
        self.new_project = 'new_project' in sys.argv
        self.return_copy_only = 'return_copy_only' in sys.argv
        self.copy_to_vm = 'copy_to_vm' in sys.argv
        self.release = 'release' in sys.argv
        self.windows_params(sys.argv)
        self.copy_self()
        script_option_dict = {'build': 'run_vbox',
                              'install': 'send_to_android',
                              'run_apk': 'run_apk'
                              }
        full_script = True
        for key, value in script_option_dict.items():
            if key in sys.argv:
                full_script = False
                print('running:  ' + value)
                getattr(self, value)()
                self.timer.how_about_now()
        if full_script:
            print('running full script')
            self.run_full_script()
        self.timer.how_about_now()

    def windows_params(self, args):
        import buildozer_tools
        bld_obj = buildozer_tools.SpecUpdater()
        bld_file = bld_obj.bld_lines + bld_obj.ext
        # bs_dict = buildozer_tools.BuildParams(bld_file).param_dict
        bld_params = buildozer_tools.BuildParams(bld_file)
        print('updated bld files')
        bs_dict = bld_params.param_dict
        jdict = buildozer_tools.BuildParams(spec='buildozer_extra.spec').param_dict
        jdict['windows_dir'] = dir_clean(os.getcwd())  # [os.getcwd().find('\\') + 1:]
        if 'vbox_dir' not in jdict or self.alt_vbox_dir is True:
            jdict['vbox_dir'] = dir_clean(os.getcwd()[os.getcwd().find('\\') + 1:])
            print('vbox_dir = ', jdict['vbox_dir'])
        jdict['package_name'] = bs_dict['package.name']
        jdict['title_unspaced'] = bs_dict['title'].replace(' ', '')
        jdict['app_name'] = bs_dict['package.domain'] + '.' + bs_dict['package.name']
        jdict['script_name'] = __file__
        if 'make_jar' in args:
            self.make_jar(jdict)
        jdict['jars_exist'] = self.jars_exist
        jdict['new_project'] = self.new_project
        jdict['return_copy_only'] = self.return_copy_only
        jdict['copy_to_vm'] = self.copy_to_vm
        jdict['release'] = self.release
        with open('script_params.json', 'w') as fp:
            json.dump(jdict, fp)
        for key, value in jdict.items():
            setattr(self, key, value)

    def make_jar(self, jdict):
        """If 'make_jar' arg is used it will compile .jar files from parameters in buildozer.spec"""
        init_dir = os.getcwd()
        try:
            if 'jar_locations' in jdict:
                jar_locations = mdir_clean(jdict['jar_locations'])
                jar_dirs = mdir_clean(jdict['jar_dirs'])
                jar_names = mdir_clean(jdict['jar_names'])
            else:
                jar_locations = [dir_clean(jdict['jar_location'])]
                jar_dirs = [dir_clean(jdict['jar_dir'])]
                jar_names = [dir_clean(jdict['jar_name'])]
            for i, jar_location in enumerate(jar_locations):
                self.jars_exist = True
                jar_dir_root = dir_clean(jdict['jar_destination_windows'])
                jar_base_dir = jar_dirs[i]
                jar_name = jar_names[i]
                split_pt = jar_base_dir[:-1].find('/')
                jar_pre = jar_base_dir[:split_pt]
                new_dir = dir_clean(os.getcwd()) + jar_dir_root + jar_pre
                jar_dir = jar_dir_root + jar_base_dir
                new_dir_long = dir_clean(os.getcwd()) + jar_dir
                if os.path.exists(jar_dir_root + jar_pre):
                    shutil.rmtree(jar_dir_root + jar_pre)
                new_root = os.getcwd() + '/' + jar_dir_root[:-1]
                if not os.path.exists(new_root):
                            print('making ' + str(new_root))
                            os.makedirs(new_root)
                jar_tree = glob.glob(jar_location + '/' + jar_pre)[0]
                print(jar_tree, new_dir)
                shutil.copytree(jar_tree, new_dir)
                dir_contents = glob.glob((new_dir_long + '/*'))
                discard_file_list = ['MainActivity', 'R.class', 'R$']
                discard_dir_list = ['/examples/']
                for file in dir_contents:
                    print(file)
                    keep = True
                    mfile = None
                    for name in discard_file_list:
                        bname = os.path.basename(file)
                        if bname[:len(name)] == name:
                            keep = False
                            break
                    if keep:
                        mfile = file.replace('\\', '/')  # depending how dir comes out
                        for dir_frag in discard_dir_list:
                            if dir_frag in mfile:
                                keep = False
                                break
                    if not keep:
                        print('removing ' + str(file))
                        os.remove(file)
                    else:
                        print('keeping:', bname, mfile, file)
                os.chdir(init_dir + '/' + jar_dir_root)
                print('\n\njarjar\n\n' + str(jar_dir))
                cmd = 'jar cf ' + jar_name + ' ' + jar_base_dir
                s = subprocess.check_output(cmd.split())
                print(str(s).replace('\\r\\n', '\r\n'))
                cmd = 'jar tf ' + jar_name
                s = subprocess.check_output(cmd.split())
                print(str(s).replace('\\r\\n', '\r\n'))
                cmd = 'jar cf ' + jar_name + ' ' + jar_base_dir
                s = subprocess.check_output(cmd.split())
                cmd = 'jar tf ' + jar_name
                s = subprocess.check_output(cmd.split())
                os.chdir(init_dir)
        except ZeroDivisionError:
        # except (AttributeError, IndexError, KeyError) as e:
            print('No jars added')
            self.jars_exist = False
            os.chdir(init_dir)
            return

    def run_full_script(self):
        self.vbox_communicative = True
        Process(target=self.run_vbox).start()
        self.await_process = Process(target=self.awaiter)
        self.await_process.start()

    def run_vbox(self):
        """Runs vbox which enters keystroke onto the VM,
        only keystrokes are copy_compile_script.py"""
        try:
            import virtualbox
            vbox = virtualbox.VirtualBox()
        except ImportError:
            import vboxapi
            vbox = virtualbox.VirtualBox()
            # print('There is an error in the pyvbox installion')
            # raise
        vm = vbox.find_machine(self.VMname)
        session = vm.create_session()
        dir_args = ' '.join(self.windows_dir.replace('\\', '/').split('/')[1:])
        run_copy_script = ("\n\npython copy_compile_script.py " + self.vbox_mount_drive +
                           ' ' + dir_args + " \n")
        try:
            session.console.keyboard.put_keys(run_copy_script)
        except AttributeError:
            print("Failed to communicate with Vbox session\n" +
                  "Vbox needs to be open in terminal mode")
            self.vbox_communicative = False

    def awaiter(self):
        """Polls the current dir awaiting an .apk with a newer file modified time
        than when initialised"""
        if self.copy_to_vm:
            return  # then nothing to await
        f_time = self.get_mtime()
        sys.stdout.write("waiting for .apk file")
        sys.stdout.flush()
        new_f_time = self.get_mtime()
        start_time = time.time()
        if self.new_project:
            wait_mins = 20
        else:
            wait_mins = 2
        incomplete = True
        count = 0
        while incomplete and (time.time() < (start_time + wait_mins * 60)):
            count += 1
            time.sleep(1)
            new_f_time = self.get_mtime()
            if new_f_time > f_time:
                incomplete = False
            sys.stdout.write(".")
            if count / 10.0 == count // 10:
                sys.stdout.write(str(count) + 's')
            sys.stdout.flush()
            if not self.vbox_communicative:
                print('... will not arrive due to Vbox failure')
                return
        if incomplete:
            print("APK file has not arrived from virtualbox after %d seconds, quitting"
                  % (wait_mins * 60))
            return
        self.timer.how_about_now()
        self.send_to_android()

    def send_to_android(self, install=True):
        """Installs and runs APK"""
        self.apk_name = max(glob.glob('*.apk'), key=os.path.getmtime)
        try:
            if install:
                print('\nSending to Android: ' + self.apk_name)
                cmd = 'adb install -r ' + self.apk_name
                s = subprocess.check_output(cmd.split())
                print(str(s).replace('\\r\\n', '\r\n'))
            print('\nRun program')
            cmd = 'adb shell am start -n ' + self.app_name.lower() + '/' + self.apk_activity
            s = subprocess.check_output(cmd.split())
            print(str(s).replace('\\r\\n', '\r\n'))
        except subprocess.CalledProcessError:
            print("error: is the phone plugged in?")
        print_help_hint()
        self.timer.how_about_now()
        print(time.ctime())

    def run_apk(self):
        """Runs apk after install"""
        self.send_to_android(install=False)

    def get_mtime(self):
        """Get file modification time"""
        file = get_latest_file('*.apk', suppress_msg=True)
        if file is not None:
            return os.path.getmtime(file)
        return 0

    def copy_self(self):
        """Creates a copy of run_compile.py to copy onto the VM"""
        full_windows = self.windows_dir
        root_dir = full_windows[:full_windows[:-1].rfind('/') + 1]
        for f in [self.script_name, 'script_params.json']:
            src = full_windows + f
            dest = root_dir + f
            print(src, dest)
            shutil.copy2(src, dest)

    def print_doc(self):
        with open('run_compile.py', 'r') as file:
            p_out = False
            line_list = []
            for line in file:
                if '#' * 20 in line:
                    p_out = not p_out
                    continue  # do not print this line
                if p_out:
                    line_list.append(line[1:])  # remove "#"
        print(''.join(line_list))


def print_help_hint():
    print('\n'*1 + '#'*50)
    print('\n Use extension: \n\n   help\n\n For a list of run_compile options\n')
    print('#'*50 + '\n'*1)


class VMbootstrap(object):
    """Bootstrap class of main script run on VM

    This class is copied into the VM from the vbox command 'python copy_compile_script.py'

    copy_compile_script.py is a short script which lives on the user/home of the VM
     - it copies:
                    run_compile.py
                    script_params.json

    All other tasks are then carried out by run_compile.py and build parameter information is
    carried in the file script_params.json
    """
    def __init__(self, windows_mount_drive):
        self.windows_mount_drive = windows_mount_drive  # '/media/sf_C_DRIVE/'

    def run(self):
        copy_to_subdir = False  # this is a switch, allows copying to subdirs on VBox
        copy_success = False
        print('script starting')
        # jdict contains all parameters and sets them as class attributes. Most of these are
        # contained in buildozer_extra.spec
        with open('script_params.json', 'r') as fp:
            jdict = json.load(fp)
        #  This sets the attrs self.vbox_dir, self.windows_dir, etc.
        for key, value in jdict.items():
            setattr(self, key, value)
        # Increase Gradle memory
        os.environ['GRADLE_OPTS'] = "-Xms1724m -Xmx5048m -Dorg.gradle.jvmargs='-Xms1724m -Xmx5048m'"
        if self.release:
            # Update P4A_RELEASE keys
            for key, value in P4A_KEYS.items():
                os.environ[key] = value
        # root directory of code on VM
        full_linux_dir = dir_clean(os.getcwd()) + dir_clean(self.vbox_dir)
        if not ch_or_mkdir(full_linux_dir, create_if_needed=self.new_project):
            print(str(full_linux_dir) + ' does not exist.\n')
            print("Use 'new_project' argument' at prompt to create new directories")
            return
        apk_path = full_linux_dir + self.buildozer_ext_path + '/'
        # apk_path = full_linux_dir + '/'
        alt_apk_path = '/home/anon/python-for-android/'
        print('Apk path is: ' + str(apk_path))
        slash_place = self.windows_dir.find(':/') + 2
        windows_dir_share = self.windows_mount_drive + self.windows_dir[slash_place:]
        dbc_print('self.windows_mount_drive', self.windows_mount_drive)
        dbc_print('self.windows_dir[slash_place:]: ' + str(self.windows_dir[slash_place:]) +
                  '\nfull self.windows_dir: ' + str(self.windows_dir))
        # Set to True when 'install' argument is used, this will skip copying and build,
        # copying .apk from specified dir onto c:/dir
        if not self.return_copy_only:  # otherwise copy all code files over
            copy_list = self.get_full_copy_dirs(windows_dir_share)
            # exclude files param is passed to glob_copy
            exclude_files = self.vbox_exclude_files.split(' ')
            for copy_option in copy_list:
                try:
                    if '/' in copy_option and copy_to_subdir:
                        additional_path = str(copy_option[:copy_option.rfind('/')+1])
                        if not os.path.isdir(full_linux_dir + additional_path):
                            os.mkdir(full_linux_dir + additional_path)
                    else:
                        additional_path = ''
                    glob_copy((windows_dir_share + copy_option), full_linux_dir +
                              additional_path, exclude_files)
                except ZeroDivisionError:
                    print('Could not copy ' + str(copy_option))
            copy_success = True
            # Execute buildozer string
            if self.copy_to_vm:  # your work here is done
                return
            if not self.release:
                subprocess_with_display(EXEC_BUILDOZER_DEBUG)
            else:
                subprocess_with_display(EXEC_BUILDOZER_RELEASE)
        # Align command has been deprecated
        # if self.release:  # .apk needs the zipalign command to go onto google play
        #     # Disabled with False
        #     release, apk = 'release', '.apk'
        #     release_file = get_latest_file(apk_path + '*' + release + apk)
        #     optimized_file = release_file[:-len(apk)] + self.release_zipalign_ext + apk
        #     full_cmd_list = [self.release_zipalign_cmd, release_file, optimized_file]
        #     print(' '.join(full_cmd_list))
        #     subprocess_with_display(full_cmd_list)
        filename = get_latest_file(apk_path + '*.apk')
        if filename is None:
            filename = get_latest_file(alt_apk_path + '*.apk')
        print('filename is: ' + str(filename))
        shutil.copy2(filename, windows_dir_share)
        return copy_success

    def get_full_copy_dirs(self, windows_dir_share):
        """Prepares a list of all directories except those specified by vbox_exclude_dir
        This prepares a list of files to copy base on wildcard + extension *.py for example,
        worth noting that exclude files is a different operation which is used to eliminate
        specific files"""
        # dbc_print('windows_dir_share', windows_dir_share)
        subdir_full_list = ([os.path.join(os.path.abspath(windows_dir_share), x[0])
                             for x in os.walk(os.path.abspath(windows_dir_share))])
        subdir_filtered_list = []
        for dir_ in subdir_full_list:
            inc = True
            for exc_dir in self.vbox_exclude_dirs:
                if exc_dir in dir_.split('/'):
                    inc = False
                    dbc_print('\n\nexcluding ' + str(exc_dir))
                    break
            if inc:
                subdir_filtered_list.append(dir_[len(windows_dir_share):] + '/')
        # dbc_print('subdir_filtered_list' + str(subdir_filtered_list))
        pre_copy_files = self.vbox_copy_files.split(' ')
        # dbc_print('pre_copy_files', pre_copy_files)
        copy_list = []
        for dir_ in subdir_filtered_list:
            for file in pre_copy_files:
                copy_list.append(dir_ + file)
        # dbc_print('copy_list', copy_list)
        return copy_list


class timer(object):
    """Used to measure operation times of compiling and copying"""

    def __init__(self):
        self.start = time.time()

    def how_about_now(self):
        time_str = "{0:.2f}".format(time.time() - self.start)
        print('Time elapsed: ' + time_str + 's')


def glob_copy(src, dest, exclude_files=None):
    """ copies files with glob * to dest
        dest should not contain filename"""
    dest_base_dir = os.path.dirname(dest)
    if not os.path.exists(dest_base_dir):
        print('making ' + str(dest_base_dir))
        os.makedirs(dest_base_dir)
    else:
        pass
        # dbc_print('already exists ' + str(dest_base_dir), dest)
    if dest[0] is not '/':
        dest = '/' + dest
    for fname in glob.iglob(src):
        if os.path.basename(fname) in exclude_files:
            dbc_print('omitting :' + os.path.basename(fname))
            continue
        dbc_print('fname:', fname)
        final_source = os.path.join(dest, os.path.basename(fname))
        dbc_print('copying ' + fname + '\n to ' + final_source)
        shutil.copy2(fname, final_source)


def get_latest_file(search_path, suppress_msg=False):
    """Searches path and uses modified time to find the latest file, returns None if empty"""
    files = glob.glob(search_path)
    if not suppress_msg:
        print('search path: ' + str(search_path) + ' yields ' + str(files))
    # print(type(files))
    try:
        if len(files) > 0:
            return max(files, key=os.path.getmtime)
    except TypeError as e:
        if not suppress_msg:
            print(e, 'search path was ' + str(search_path))


def dir_clean(init_str, leader=False, trailer=True):
    """replaces \\ with /, adds a / leader if needed, adds a trailing / if asked for.
    Use mdir_clean for a list of strings"""
    if len(init_str) == 0:
        return init_str
    init_str = init_str.replace('\\', '/')
    if leader and (init_str[0] != "/"):
        init_str = "/" + init_str
    if trailer and (init_str[-1:] != "/"):
        init_str += "/"
    return init_str


def mdir_clean(init_str_list, leader=False, trailer=True):
    """applies dir_clean to a list"""
    clean_list = []
    for dir_str in init_str_list:
        clean_list.append(dir_clean(dir_str, leader, trailer))
    return clean_list


def ch_or_mkdir(dir_str, create_if_needed=True):
    """Changes working directory, creating the dir if needed"""
    dir_str = dir_clean(dir_str)
    cur_dir = dir_clean(os.getcwd())
    if cur_dir in dir_str:
        dir_extension = dir_str[len(cur_dir):]
    else:  # root not matching up, give it a single try
        return _ch_or_mkdir(dir_str, create_if_needed)
    for d in dir_extension.split('/'):
        new_dir_str = cur_dir + d
        successful = _ch_or_mkdir(new_dir_str, create_if_needed)
        if not successful:
            return False
        # print(cur_dir, type(str(d)), str(d))
        cur_dir = dir_clean(cur_dir + str(d))
    return True


def _ch_or_mkdir(dir_str, create_if_needed=True):
    """Sub command used by ch_or_mkdir, not to be exectuted by itself"""
    try:
        os.chdir(dir_str)
    except OSError:
        if create_if_needed:
            print(dir_str + " does not exist, attempting creation")
            try:
                os.mkdir(dir_str)
                os.chdir(dir_str)
            except OSError as e:
                print(e)
                print(dir_str + " does not exist, and could not be created")
                return False
        else:
            print(dir_str + " does not exist, use 'new_project' arg from run_compile.py" +
                            " to create this directory")
            return False
    return True


def subprocess_with_display(cmd_list, args=None):
    """Executes subprocess from cmd displaying output """
    # cmd_str = ' '.join(cmd_list + ['--outbuf=L'])
    print(cmd_list + ['--outbuf=L'])
    # p = subprocess.Popen(cmd_list, stdout=subprocess.PIPE, env=dict(os.environ))
    p = subprocess.Popen(cmd_list, stdout=subprocess.PIPE)
    if args:
        p.communicate(input='y')
    for line in iter(p.stdout.readline, b''):
        print(('{}'.format(line.rstrip()))[2:])


def dbc_print(*args):
    """Used for printing information on file copies, set turned on to True"""
    turned_on = False
    if turned_on:
        for arg in args:
            if not hasattr(arg, '__iter__') or isinstance(arg, str):
                print(arg)
            else:
                for a in arg:
                    print(a)


if __name__ == "__main__":
    s = VboxScript()
