import shutil
import os
import sys


def copy_and_compile(mount_drive_string, dir_list):
    mount_drive = '/media/sf_' + mount_drive_string + '_DRIVE/'
    flist = ['run_compile.py', 'script_params.json']
    src_path = mount_drive + '/'.join(dir_list) + '/'
    for f in flist:
        # shutil.copy2((mount_drive + dir_list'test1/test2/' + f), f)
        shutil.copy2((src_path + f), f)
    import run_compile
    print('file copy completed')
    s = run_compile.VMbootstrap(mount_drive)
    start_dir = os.getcwd() + '/'
    if s.run():
        for f in flist:
            os.remove(start_dir + f)

if __name__ == '__main__':
    # Parse the arguments
    args = sys.argv
    dir_list = sys.argv[2:]
    copy_and_compile(sys.argv[1], dir_list)
 